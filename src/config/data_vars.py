# NASA IDS Project
#
# Copyright (c) 2019 Wu Sun <wsun@carnegiescience.edu>
# Michalak Lab
# Department of Global Ecology
# Carnegie Institution for Science
"""Data variables for regression analysis."""
from typing import Dict, List

# NOTE: This dictionary of data variables was generated after running the
# regressions and may change in the future.
# NOTE: Use black for reformatting because it is consistent with JSON style.
#
# excluded models:
# 1. MsTMIP v2: JPL-CENTURY (no GPP)
# 2. TRENDY v6: LPJ-GUESS (no NEE)
# 3. SIF: GOME-2B, GOSAT, OCO-2 (poor coverage of raw data)
DATA_VARS: Dict[str, List[str]] = {
    "mdl_vars": [
        "PRmodel-1::GPP",
        "PRmodel-2::GPP",
        "PRmodel-3::GPP",
        "PRmodel-4::GPP",
        "PRmodel-5::GPP",
        "PRmodel-6::GPP",
        "P-model-1::GPP",
        "P-model-2::GPP",
        "VPM::GPP",
        "MODc55::GPP",
        "MODc6::GPP",
        "JPL-MODIS::GPP",
        "BESS::GPP",
        "BEPS::GPP",
        "GOME2A::SIF",
        # "GOME2B::SIF",
        # "GOSAT::SIF",
        # "OCO2::SIF",
        "GOME2A-kriged::SIF",
        "RSIF::SIF",
        "CSIF::SIF",
        "FluxCom-ANN::GPP",
        "FluxCom-MARS::GPP",
        "FluxCom-RF::GPP",
        "FluxCom-ANN::NEE",
        "FluxCom-MARS::NEE",
        "FluxCom-RF::NEE",
        "mstmip-v1.BIOME-BGC::GPP",
        "mstmip-v1.CLM4::GPP",
        "mstmip-v1.CLM4VIC::GPP",
        "mstmip-v1.DLEM::GPP",
        "mstmip-v1.GTEC::GPP",
        "mstmip-v1.ISAM::GPP",
        "mstmip-v1.LPJ-wsl::GPP",
        "mstmip-v1.ORCHIDEE-LSCE::GPP",
        "mstmip-v1.VEGAS::GPP",
        "mstmip-v1.VISIT::GPP",
        "mstmip-v1.BIOME-BGC::NEE",
        "mstmip-v1.CLM4::NEE",
        "mstmip-v1.CLM4VIC::NEE",
        "mstmip-v1.DLEM::NEE",
        "mstmip-v1.GTEC::NEE",
        "mstmip-v1.ISAM::NEE",
        "mstmip-v1.LPJ-wsl::NEE",
        "mstmip-v1.ORCHIDEE-LSCE::NEE",
        "mstmip-v1.VEGAS::NEE",
        "mstmip-v1.VISIT::NEE",
        "mstmip-v2.BIOME-BGC::GPP",
        "mstmip-v2.CLASS-CTEM-N::GPP",
        "mstmip-v2.CLM4VIC::GPP",
        "mstmip-v2.CLM4::GPP",
        "mstmip-v2.DLEM::GPP",
        "mstmip-v2.GTEC::GPP",
        "mstmip-v2.ISAM::GPP",
        "mstmip-v2.JPL-HYLAND::GPP",
        "mstmip-v2.LPJ-wsl::GPP",
        "mstmip-v2.ORCHIDEE-LSCE::GPP",
        "mstmip-v2.SiB3::GPP",
        "mstmip-v2.SiBCASA::GPP",
        "mstmip-v2.TEM6::GPP",
        "mstmip-v2.TRIPLEX-GHG::GPP",
        "mstmip-v2.VEGAS2.1::GPP",
        "mstmip-v2.VISIT::GPP",
        "mstmip-v2.BIOME-BGC::NEE",
        "mstmip-v2.CLASS-CTEM-N::NEE",
        "mstmip-v2.CLM4VIC::NEE",
        "mstmip-v2.CLM4::NEE",
        "mstmip-v2.DLEM::NEE",
        "mstmip-v2.GTEC::NEE",
        "mstmip-v2.ISAM::NEE",
        # "mstmip-v2.JPL-CENTURY::NEE",
        "mstmip-v2.JPL-HYLAND::NEE",
        "mstmip-v2.LPJ-wsl::NEE",
        "mstmip-v2.ORCHIDEE-LSCE::NEE",
        "mstmip-v2.SiB3::NEE",
        "mstmip-v2.SiBCASA::NEE",
        "mstmip-v2.TEM6::NEE",
        "mstmip-v2.TRIPLEX-GHG::NEE",
        "mstmip-v2.VEGAS2.1::NEE",
        "mstmip-v2.VISIT::NEE",
        "trendy-v5.CABLE::GPP",
        "trendy-v5.CLASS-CTEM::GPP",
        "trendy-v5.CLM::GPP",
        "trendy-v5.DLEM::GPP",
        "trendy-v5.ISAM::GPP",
        "trendy-v5.JSBACH::GPP",
        "trendy-v5.JULES::GPP",
        "trendy-v5.LPJ::GPP",
        "trendy-v5.LPX-Bern::GPP",
        "trendy-v5.ORCHIDEE::GPP",
        "trendy-v5.SDGVM::GPP",
        "trendy-v5.VEGAS::GPP",
        "trendy-v5.VISIT::GPP",
        "trendy-v6.CABLE::GPP",
        "trendy-v6.CLASS-CTEM::GPP",
        "trendy-v6.CLM4.5::GPP",
        "trendy-v6.DLEM::GPP",
        "trendy-v6.ISAM::GPP",
        "trendy-v6.JULES::GPP",
        # "trendy-v6.LPJ-GUESS::GPP",
        "trendy-v6.LPJ-wsl::GPP",
        "trendy-v6.OCN::GPP",
        "trendy-v6.ORCHIDEE-MICT::GPP",
        "trendy-v6.ORCHIDEE::GPP",
        "trendy-v6.SDGVM::GPP",
        "trendy-v6.VEGAS::GPP",
        "trendy-v6.VISIT::GPP",
        "trendy-v6.CABLE::NEE",
        "trendy-v6.CLASS-CTEM::NEE",
        "trendy-v6.CLM4.5::NEE",
        "trendy-v6.DLEM::NEE",
        "trendy-v6.ISAM::NEE",
        "trendy-v6.JULES::NEE",
        "trendy-v6.LPJ-wsl::NEE",
        "trendy-v6.OCN::NEE",
        "trendy-v6.ORCHIDEE-MICT::NEE",
        "trendy-v6.ORCHIDEE::NEE",
        "trendy-v6.SDGVM::NEE",
        "trendy-v6.VEGAS::NEE",
        "trendy-v6.VISIT::NEE",
        "PRmodel-1::LUE",
        "PRmodel-2::LUE",
        "PRmodel-3::LUE",
        "PRmodel-4::LUE",
        "PRmodel-5::LUE",
        "PRmodel-6::LUE",
        "P-model-1::LUE",
        "P-model-2::LUE",
        "JPL-MODIS::LUE",
        "VPM::LUE",
        "MODc55::LUE",
        "MODc6::LUE",
        "BESS::LUE",
        "BEPS::LUE",
    ],
    "mdl_vars_ids": [
        "PRmodel-1::GPP",
        "PRmodel-2::GPP",
        "PRmodel-3::GPP",
        "PRmodel-4::GPP",
        "PRmodel-5::GPP",
        "PRmodel-6::GPP",
        "P-model-1::GPP",
        "P-model-2::GPP",
        "VPM::GPP",
        "MODc55::GPP",
        "MODc6::GPP",
        "JPL-MODIS::GPP",
        "BESS::GPP",
        "BEPS::GPP",
    ],
    "mdl_vars_lue": [
        "PRmodel-1::LUE",
        "PRmodel-2::LUE",
        "PRmodel-3::LUE",
        "PRmodel-4::LUE",
        "PRmodel-5::LUE",
        "PRmodel-6::LUE",
        "P-model-1::LUE",
        "P-model-2::LUE",
        "VPM::LUE",
        "MODc55::LUE",
        "MODc6::LUE",
        "JPL-MODIS::LUE",
        "BESS::LUE",
        "BEPS::LUE",
    ],
    "mdl_vars_sif": [
        "GOME2A::SIF",
        # "GOME2B::SIF",
        # "GOSAT::SIF",
        # "OCO2::SIF",
        "GOME2A-kriged::SIF",
        "RSIF::SIF",
        "CSIF::SIF",
    ],
    "mdl_vars_fluxcom": [
        "FluxCom-ANN::GPP",
        "FluxCom-MARS::GPP",
        "FluxCom-RF::GPP",
        "FluxCom-ANN::NEE",
        "FluxCom-MARS::NEE",
        "FluxCom-RF::NEE",
    ],
    "mdl_vars_mstmip_v1": [
        "mstmip-v1.BIOME-BGC::GPP",
        "mstmip-v1.CLM4::GPP",
        "mstmip-v1.CLM4VIC::GPP",
        "mstmip-v1.DLEM::GPP",
        "mstmip-v1.GTEC::GPP",
        "mstmip-v1.ISAM::GPP",
        "mstmip-v1.LPJ-wsl::GPP",
        "mstmip-v1.ORCHIDEE-LSCE::GPP",
        "mstmip-v1.VEGAS::GPP",
        "mstmip-v1.VISIT::GPP",
        "mstmip-v1.BIOME-BGC::NEE",
        "mstmip-v1.CLM4::NEE",
        "mstmip-v1.CLM4VIC::NEE",
        "mstmip-v1.DLEM::NEE",
        "mstmip-v1.GTEC::NEE",
        "mstmip-v1.ISAM::NEE",
        "mstmip-v1.LPJ-wsl::NEE",
        "mstmip-v1.ORCHIDEE-LSCE::NEE",
        "mstmip-v1.VEGAS::NEE",
        "mstmip-v1.VISIT::NEE",
    ],
    "mdl_vars_mstmip_v2": [
        "mstmip-v2.BIOME-BGC::GPP",
        "mstmip-v2.CLASS-CTEM-N::GPP",
        "mstmip-v2.CLM4VIC::GPP",
        "mstmip-v2.CLM4::GPP",
        "mstmip-v2.DLEM::GPP",
        "mstmip-v2.GTEC::GPP",
        "mstmip-v2.ISAM::GPP",
        "mstmip-v2.JPL-HYLAND::GPP",
        "mstmip-v2.LPJ-wsl::GPP",
        "mstmip-v2.ORCHIDEE-LSCE::GPP",
        "mstmip-v2.SiB3::GPP",
        "mstmip-v2.SiBCASA::GPP",
        "mstmip-v2.TEM6::GPP",
        "mstmip-v2.TRIPLEX-GHG::GPP",
        "mstmip-v2.VEGAS2.1::GPP",
        "mstmip-v2.VISIT::GPP",
        "mstmip-v2.BIOME-BGC::NEE",
        "mstmip-v2.CLASS-CTEM-N::NEE",
        "mstmip-v2.CLM4VIC::NEE",
        "mstmip-v2.CLM4::NEE",
        "mstmip-v2.DLEM::NEE",
        "mstmip-v2.GTEC::NEE",
        "mstmip-v2.ISAM::NEE",
        # "mstmip-v2.JPL-CENTURY::NEE",
        "mstmip-v2.JPL-HYLAND::NEE",
        "mstmip-v2.LPJ-wsl::NEE",
        "mstmip-v2.ORCHIDEE-LSCE::NEE",
        "mstmip-v2.SiB3::NEE",
        "mstmip-v2.SiBCASA::NEE",
        "mstmip-v2.TEM6::NEE",
        "mstmip-v2.TRIPLEX-GHG::NEE",
        "mstmip-v2.VEGAS2.1::NEE",
        "mstmip-v2.VISIT::NEE",
    ],
    "mdl_vars_trendy_v5": [
        "trendy-v5.CABLE::GPP",
        "trendy-v5.CLASS-CTEM::GPP",
        "trendy-v5.CLM::GPP",
        "trendy-v5.DLEM::GPP",
        "trendy-v5.ISAM::GPP",
        "trendy-v5.JSBACH::GPP",
        "trendy-v5.JULES::GPP",
        "trendy-v5.LPJ::GPP",
        "trendy-v5.LPX-Bern::GPP",
        "trendy-v5.ORCHIDEE::GPP",
        "trendy-v5.SDGVM::GPP",
        "trendy-v5.VEGAS::GPP",
        "trendy-v5.VISIT::GPP",
    ],
    "mdl_vars_trendy_v6": [
        "trendy-v6.CABLE::GPP",
        "trendy-v6.CLASS-CTEM::GPP",
        "trendy-v6.CLM4.5::GPP",
        "trendy-v6.DLEM::GPP",
        "trendy-v6.ISAM::GPP",
        "trendy-v6.JULES::GPP",
        # "trendy-v6.LPJ-GUESS::GPP",
        "trendy-v6.LPJ-wsl::GPP",
        "trendy-v6.OCN::GPP",
        "trendy-v6.ORCHIDEE-MICT::GPP",
        "trendy-v6.ORCHIDEE::GPP",
        "trendy-v6.SDGVM::GPP",
        "trendy-v6.VEGAS::GPP",
        "trendy-v6.VISIT::GPP",
        "trendy-v6.CABLE::NEE",
        "trendy-v6.CLASS-CTEM::NEE",
        "trendy-v6.CLM4.5::NEE",
        "trendy-v6.DLEM::NEE",
        "trendy-v6.ISAM::NEE",
        "trendy-v6.JULES::NEE",
        "trendy-v6.LPJ-wsl::NEE",
        "trendy-v6.OCN::NEE",
        "trendy-v6.ORCHIDEE-MICT::NEE",
        "trendy-v6.ORCHIDEE::NEE",
        "trendy-v6.SDGVM::NEE",
        "trendy-v6.VEGAS::NEE",
        "trendy-v6.VISIT::NEE",
    ],
    "mdl_vars_mstmip": [
        "mstmip-v1.BIOME-BGC::GPP",
        "mstmip-v1.CLM4::GPP",
        "mstmip-v1.CLM4VIC::GPP",
        "mstmip-v1.DLEM::GPP",
        "mstmip-v1.GTEC::GPP",
        "mstmip-v1.ISAM::GPP",
        "mstmip-v1.LPJ-wsl::GPP",
        "mstmip-v1.ORCHIDEE-LSCE::GPP",
        "mstmip-v1.VEGAS::GPP",
        "mstmip-v1.VISIT::GPP",
        "mstmip-v1.BIOME-BGC::NEE",
        "mstmip-v1.CLM4::NEE",
        "mstmip-v1.CLM4VIC::NEE",
        "mstmip-v1.DLEM::NEE",
        "mstmip-v1.GTEC::NEE",
        "mstmip-v1.ISAM::NEE",
        "mstmip-v1.LPJ-wsl::NEE",
        "mstmip-v1.ORCHIDEE-LSCE::NEE",
        "mstmip-v1.VEGAS::NEE",
        "mstmip-v1.VISIT::NEE",
        "mstmip-v2.BIOME-BGC::GPP",
        "mstmip-v2.CLASS-CTEM-N::GPP",
        "mstmip-v2.CLM4VIC::GPP",
        "mstmip-v2.CLM4::GPP",
        "mstmip-v2.DLEM::GPP",
        "mstmip-v2.GTEC::GPP",
        "mstmip-v2.ISAM::GPP",
        "mstmip-v2.JPL-HYLAND::GPP",
        "mstmip-v2.LPJ-wsl::GPP",
        "mstmip-v2.ORCHIDEE-LSCE::GPP",
        "mstmip-v2.SiB3::GPP",
        "mstmip-v2.SiBCASA::GPP",
        "mstmip-v2.TEM6::GPP",
        "mstmip-v2.TRIPLEX-GHG::GPP",
        "mstmip-v2.VEGAS2.1::GPP",
        "mstmip-v2.VISIT::GPP",
        "mstmip-v2.BIOME-BGC::NEE",
        "mstmip-v2.CLASS-CTEM-N::NEE",
        "mstmip-v2.CLM4VIC::NEE",
        "mstmip-v2.CLM4::NEE",
        "mstmip-v2.DLEM::NEE",
        "mstmip-v2.GTEC::NEE",
        "mstmip-v2.ISAM::NEE",
        # "mstmip-v2.JPL-CENTURY::NEE",
        "mstmip-v2.JPL-HYLAND::NEE",
        "mstmip-v2.LPJ-wsl::NEE",
        "mstmip-v2.ORCHIDEE-LSCE::NEE",
        "mstmip-v2.SiB3::NEE",
        "mstmip-v2.SiBCASA::NEE",
        "mstmip-v2.TEM6::NEE",
        "mstmip-v2.TRIPLEX-GHG::NEE",
        "mstmip-v2.VEGAS2.1::NEE",
        "mstmip-v2.VISIT::NEE",
    ],
    "mdl_vars_trendy": [
        "trendy-v5.CABLE::GPP",
        "trendy-v5.CLASS-CTEM::GPP",
        "trendy-v5.CLM::GPP",
        "trendy-v5.DLEM::GPP",
        "trendy-v5.ISAM::GPP",
        "trendy-v5.JSBACH::GPP",
        "trendy-v5.JULES::GPP",
        "trendy-v5.LPJ::GPP",
        "trendy-v5.LPX-Bern::GPP",
        "trendy-v5.ORCHIDEE::GPP",
        "trendy-v5.SDGVM::GPP",
        "trendy-v5.VEGAS::GPP",
        "trendy-v5.VISIT::GPP",
        "trendy-v6.CABLE::GPP",
        "trendy-v6.CLASS-CTEM::GPP",
        "trendy-v6.CLM4.5::GPP",
        "trendy-v6.DLEM::GPP",
        "trendy-v6.ISAM::GPP",
        "trendy-v6.JULES::GPP",
        # "trendy-v6.LPJ-GUESS::GPP",
        "trendy-v6.LPJ-wsl::GPP",
        "trendy-v6.OCN::GPP",
        "trendy-v6.ORCHIDEE-MICT::GPP",
        "trendy-v6.ORCHIDEE::GPP",
        "trendy-v6.SDGVM::GPP",
        "trendy-v6.VEGAS::GPP",
        "trendy-v6.VISIT::GPP",
        "trendy-v6.CABLE::NEE",
        "trendy-v6.CLASS-CTEM::NEE",
        "trendy-v6.CLM4.5::NEE",
        "trendy-v6.DLEM::NEE",
        "trendy-v6.ISAM::NEE",
        "trendy-v6.JULES::NEE",
        "trendy-v6.LPJ-wsl::NEE",
        "trendy-v6.OCN::NEE",
        "trendy-v6.ORCHIDEE-MICT::NEE",
        "trendy-v6.ORCHIDEE::NEE",
        "trendy-v6.SDGVM::NEE",
        "trendy-v6.VEGAS::NEE",
        "trendy-v6.VISIT::NEE",
    ],
    "mdl_vars_prog": [
        "mstmip-v1.BIOME-BGC::GPP",
        "mstmip-v1.CLM4::GPP",
        "mstmip-v1.CLM4VIC::GPP",
        "mstmip-v1.DLEM::GPP",
        "mstmip-v1.GTEC::GPP",
        "mstmip-v1.ISAM::GPP",
        "mstmip-v1.LPJ-wsl::GPP",
        "mstmip-v1.ORCHIDEE-LSCE::GPP",
        "mstmip-v1.VEGAS::GPP",
        "mstmip-v1.VISIT::GPP",
        "mstmip-v1.BIOME-BGC::NEE",
        "mstmip-v1.CLM4::NEE",
        "mstmip-v1.CLM4VIC::NEE",
        "mstmip-v1.DLEM::NEE",
        "mstmip-v1.GTEC::NEE",
        "mstmip-v1.ISAM::NEE",
        "mstmip-v1.LPJ-wsl::NEE",
        "mstmip-v1.ORCHIDEE-LSCE::NEE",
        "mstmip-v1.VEGAS::NEE",
        "mstmip-v1.VISIT::NEE",
        "mstmip-v2.BIOME-BGC::GPP",
        "mstmip-v2.CLASS-CTEM-N::GPP",
        "mstmip-v2.CLM4VIC::GPP",
        "mstmip-v2.CLM4::GPP",
        "mstmip-v2.DLEM::GPP",
        "mstmip-v2.GTEC::GPP",
        "mstmip-v2.ISAM::GPP",
        "mstmip-v2.JPL-HYLAND::GPP",
        "mstmip-v2.LPJ-wsl::GPP",
        "mstmip-v2.ORCHIDEE-LSCE::GPP",
        "mstmip-v2.SiB3::GPP",
        "mstmip-v2.SiBCASA::GPP",
        "mstmip-v2.TEM6::GPP",
        "mstmip-v2.TRIPLEX-GHG::GPP",
        "mstmip-v2.VEGAS2.1::GPP",
        "mstmip-v2.VISIT::GPP",
        "mstmip-v2.BIOME-BGC::NEE",
        "mstmip-v2.CLASS-CTEM-N::NEE",
        "mstmip-v2.CLM4VIC::NEE",
        "mstmip-v2.CLM4::NEE",
        "mstmip-v2.DLEM::NEE",
        "mstmip-v2.GTEC::NEE",
        "mstmip-v2.ISAM::NEE",
        # "mstmip-v2.JPL-CENTURY::NEE",
        "mstmip-v2.JPL-HYLAND::NEE",
        "mstmip-v2.LPJ-wsl::NEE",
        "mstmip-v2.ORCHIDEE-LSCE::NEE",
        "mstmip-v2.SiB3::NEE",
        "mstmip-v2.SiBCASA::NEE",
        "mstmip-v2.TEM6::NEE",
        "mstmip-v2.TRIPLEX-GHG::NEE",
        "mstmip-v2.VEGAS2.1::NEE",
        "mstmip-v2.VISIT::NEE",
        "trendy-v5.CABLE::GPP",
        "trendy-v5.CLASS-CTEM::GPP",
        "trendy-v5.CLM::GPP",
        "trendy-v5.DLEM::GPP",
        "trendy-v5.ISAM::GPP",
        "trendy-v5.JSBACH::GPP",
        "trendy-v5.JULES::GPP",
        "trendy-v5.LPJ::GPP",
        "trendy-v5.LPX-Bern::GPP",
        "trendy-v5.ORCHIDEE::GPP",
        "trendy-v5.SDGVM::GPP",
        "trendy-v5.VEGAS::GPP",
        "trendy-v5.VISIT::GPP",
        "trendy-v6.CABLE::GPP",
        "trendy-v6.CLASS-CTEM::GPP",
        "trendy-v6.CLM4.5::GPP",
        "trendy-v6.DLEM::GPP",
        "trendy-v6.ISAM::GPP",
        "trendy-v6.JULES::GPP",
        # "trendy-v6.LPJ-GUESS::GPP",
        "trendy-v6.LPJ-wsl::GPP",
        "trendy-v6.OCN::GPP",
        "trendy-v6.ORCHIDEE-MICT::GPP",
        "trendy-v6.ORCHIDEE::GPP",
        "trendy-v6.SDGVM::GPP",
        "trendy-v6.VEGAS::GPP",
        "trendy-v6.VISIT::GPP",
        "trendy-v6.CABLE::NEE",
        "trendy-v6.CLASS-CTEM::NEE",
        "trendy-v6.CLM4.5::NEE",
        "trendy-v6.DLEM::NEE",
        "trendy-v6.ISAM::NEE",
        "trendy-v6.JULES::NEE",
        "trendy-v6.LPJ-wsl::NEE",
        "trendy-v6.OCN::NEE",
        "trendy-v6.ORCHIDEE-MICT::NEE",
        "trendy-v6.ORCHIDEE::NEE",
        "trendy-v6.SDGVM::NEE",
        "trendy-v6.VEGAS::NEE",
        "trendy-v6.VISIT::NEE",
    ],
    "aux_vars": [
        "EVI",
        "LAI",
        "NDVI",
        "NIRv",
        "SWIR",
        "fAPAR",
        "temp",
        "rel_hum",
        "sp_hum",
        "precip",
        "soil_moisture",
        "R_sw",
        "vpd",
        "precip_16d",
        "precip_30d",
        "alpha",
        "APAR",
    ],
    "aux_vars_veg": ["EVI", "LAI", "NDVI", "NIRv", "SWIR", "fAPAR"],
    "aux_vars_clim": [
        "temp",
        "rel_hum",
        "sp_hum",
        "precip",
        "soil_moisture",
        "R_sw",
        "vpd",
        "precip_16d",
        "precip_30d",
        "alpha",
        "APAR",
    ],
}
