# NASA IDS Project
#
# Copyright (c) 2019 Wu Sun <wsun@carnegiescience.edu>
# Michalak Lab
# Department of Global Ecology
# Carnegie Institution for Science
"""Preprocessing functions."""
from typing import Dict, Optional

import h5py
import numpy as np
from scipy.io import loadmat as _loadmat

__all__ = ["read_mat"]


def _read_mat_hdf(
    path: str, transpose: bool = True
) -> Optional[Dict[str, np.ndarray]]:
    """
    Read MAT-file (v7.3).

    Parameters
    ----------
    path : str
        A file path to read from.
    transpose : bool, optional
        Transpose column-major HDF5 array to row-major C array (NumPy default).
        Default is True.

    Returns
    -------
    res : dict or None
        Results read from the MAT-file. Return ``None`` if data reading fails.

    """
    try:
        with h5py.File(path, "r") as f:
            keys = list(f.keys())
            if transpose:
                return {k: f[k][()].T for k in keys}
            else:
                return {k: f[k][()] for k in keys}
    except OSError:
        return None


def read_mat(
    path: str, transpose_hdf5: bool = True
) -> Optional[Dict[str, np.ndarray]]:
    """
    Read MAT-file. Support both the old format and the new HDF5-based format.

    Parameters
    ----------
    path : str
        A file path to read from.
    transpose_hdf5 : bool, optional
        Transpose column-major HDF5 array to row-major C array (NumPy default).
        Default is True. This option only applies to MAT-file v7.3.

    Returns
    -------
    res : dict or None
        Results read from the MAT-file. If data loading fails, return None.

    """
    try:
        mat_dict = _loadmat(path)
        res: Dict[str, np.ndarray] = {
            k: v for k, v in mat_dict.items() if not k.startswith("__")
        }
        return res
    except NotImplementedError:
        # call HDF5 reader if SciPy reader fails
        return _read_mat_hdf(path, transpose=transpose_hdf5)
