# NASA IDS Project
#
# Copyright (c) 2019--2020 Wu Sun <wsun@carnegiescience.edu>
# Michalak Lab
# Department of Global Ecology
# Carnegie Institution for Science
"""Plotting functions."""
from typing import List, Optional, Tuple

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import patches, lines
from matplotlib.colors import rgb2hex

from config.data_vars import DATA_VARS


def _set_color_number(var: str) -> int:
    """Set color numbers (tab20 palette) for data variables on an R^2 plot."""
    if var in DATA_VARS["mdl_vars_ids"]:
        return 0
    elif var in DATA_VARS["mdl_vars_lue"]:
        return 18
    elif var in DATA_VARS["mdl_vars_sif"]:
        return 4
    elif var in DATA_VARS["mdl_vars_fluxcom"]:
        return 2 if "::GPP" in var else 3
    elif var in DATA_VARS["mdl_vars_mstmip_v1"]:
        return 8 if "::GPP" in var else 9
    elif var in DATA_VARS["mdl_vars_mstmip_v2"]:
        return 12 if "::GPP" in var else 13
    elif var in DATA_VARS["mdl_vars_trendy_v5"]:
        return 16 if "::GPP" in var else 17
    elif var in DATA_VARS["mdl_vars_trendy_v6"]:
        return 10 if "::GPP" in var else 11
    else:
        return 15  # light gray


def _set_positions() -> List[float]:
    """Set positions of data variables on an R^2 plot."""
    positions: List[float] = [
        float(i + 3)
        for i in range(
            len(DATA_VARS["mdl_vars_ids"][8:]) + len(DATA_VARS["mdl_vars_sif"])
        )
    ]
    prmodel_positions: List[float] = [(i - 1.5) / 3.0 for i in range(6)]
    p_model_positions: List[float] = [
        np.floor(prmodel_positions[-1]) + 0.75,
        np.floor(prmodel_positions[-1]) + 1.25,
    ]
    positions = prmodel_positions + p_model_positions + positions

    # FluxCom models
    cur_pos: float = positions[-1] + 1
    mdl_vars_fluxcom: List[str] = DATA_VARS["mdl_vars_fluxcom"]
    unique_mdl_vars_fluxcom: List[str] = sorted(
        list({m.split("::")[0] for m in mdl_vars_fluxcom})
    )
    positions += [
        cur_pos + unique_mdl_vars_fluxcom.index(m.split("::")[0])
        for m in mdl_vars_fluxcom
    ]

    # MsTMIP v1 models (centroid: 18.25)
    cur_pos = positions[-1] + 1
    mdl_vars_mstmip_v1: List[str] = DATA_VARS["mdl_vars_mstmip_v1"]
    unique_mdl_vars_mstmip_v1: List[str] = sorted(
        list({m.split("::")[0] for m in mdl_vars_mstmip_v1})
    )
    positions += [
        cur_pos + unique_mdl_vars_mstmip_v1.index(m.split("::")[0]) * 0.5
        for m in mdl_vars_mstmip_v1
    ]

    # MsTMIP v2 models (centroid: 25.75)
    cur_pos = np.ceil(positions[-1]) + 1
    mdl_vars_mstmip_v2: List[str] = DATA_VARS["mdl_vars_mstmip_v2"]
    unique_mdl_vars_mstmip_v2: List[str] = sorted(
        list({m.split("::")[0] for m in mdl_vars_mstmip_v2})
    )
    positions += [
        cur_pos + unique_mdl_vars_mstmip_v2.index(m.split("::")[0]) * 0.5
        for m in mdl_vars_mstmip_v2
    ]

    # TRENDY v5 models (centroid: 34.0)
    cur_pos = np.ceil(positions[-1]) + 1
    mdl_vars_trendy_v5: List[str] = DATA_VARS["mdl_vars_trendy_v5"]
    unique_mdl_vars_trendy_v5: List[str] = sorted(
        list({m.split("::")[0] for m in mdl_vars_trendy_v5})
    )
    positions += [
        cur_pos + unique_mdl_vars_trendy_v5.index(m.split("::")[0]) * 0.5
        for m in mdl_vars_trendy_v5
    ]

    # TRENDY v6 models (centroid: 41.0)
    cur_pos = np.ceil(positions[-1]) + 1
    mdl_vars_trendy_v6: List[str] = DATA_VARS["mdl_vars_trendy_v6"]
    unique_mdl_vars_trendy_v6: List[str] = sorted(
        list({m.split("::")[0] for m in mdl_vars_trendy_v6})
    )
    positions += [
        cur_pos + unique_mdl_vars_trendy_v6.index(m.split("::")[0]) * 0.5
        for m in mdl_vars_trendy_v6
    ]

    # append LUE positions
    positions += (
        prmodel_positions
        + p_model_positions
        + [float(i + 3) for i in range(len(DATA_VARS["mdl_vars_ids"][8:]))]
    )

    return positions


def _make_marker_styles():
    """Generate a list of positions and marker styles for variables."""
    var_names: List[str] = DATA_VARS["mdl_vars"]
    var_color_numbers: List[int] = [_set_color_number(v) for v in var_names]
    var_colors: List[str] = [
        rgb2hex(plt.cm.tab20(k)) for k in var_color_numbers
    ]
    var_pos: List[float] = _set_positions()
    assert len(var_pos) == len(var_names)

    var_markers: List[str] = (
        ["o"] * 8  # PRmodel & P-model
        + ["o"] * 6  # other IDS GPP
        + [">"] * 4  # SIF
        + ["o"] * 3  # FluxCom GPP
        + ["s"] * 3  # FluxCom NEE
        + [
            "o" if m.endswith("::GPP") else "s"
            for m in DATA_VARS["mdl_vars_mstmip"]
            + DATA_VARS["mdl_vars_trendy"]
        ]
        + ["<"] * 8  # PRmodel & P-model LUE
        + ["<"] * 6  # other IDS LUE
    )
    assert len(var_markers) == len(var_names)
    var_markersizes: List[float] = []
    for m in var_names:
        if m.startswith("PRmodel"):
            var_markersizes.append(5)
        elif m.startswith("P-model"):
            var_markersizes.append(10)
        else:
            var_markersizes.append(20)

    return pd.DataFrame(
        {
            "var_name": var_names,
            "color": var_colors,
            "marker": var_markers,
            "markersize": var_markersizes,
            "position": var_pos,
        }
    )


def _make_axis_labels():
    """Generate a list of axis labels for model variables on a R^2 plot."""
    var_names_unique: List[str] = [
        "PRmodel",
        "P-model",
        "VPM",
        "MODc55",
        "MODc6",
        "JPL-MODIS",
        "BESS",
        "BEPS",
        "GOME-2A",
        # "GOME-2B",
        # "GOSAT",
        # "OCO-2",
        "GOME-2A-kriged",
        "RSIF",
        "CSIF",
        "FluxCom-ANN",
        "FluxCom-MARS",
        "FluxCom-RF",
        "MsTMIP-v1\nGPP & NEE",
        "MsTMIP-v2\nGPP & NEE",
        "TRENDY-v5\nGPP",
        "TRENDY-v6\nGPP & NEE",
    ]  # note: must match the order in config.data_vars
    var_pos_unique: List[float] = [
        float(i + 3)
        for i in range(
            len(DATA_VARS["mdl_vars_ids"][8:])
            + len(DATA_VARS["mdl_vars_sif"])
            + len(DATA_VARS["mdl_vars_fluxcom"]) // 2
        )
    ]
    var_pos_unique = [0.5, 2.0] + var_pos_unique
    offset: float = var_pos_unique[-1] + 1.0
    var_pos_unique += [
        offset + 2.25,
        offset + 9.75,
        offset + 18,
        offset + 25,
    ]

    return pd.DataFrame(
        {"label": var_names_unique, "position": var_pos_unique}
    )


def _make_legend_handles():
    """Make legend handles for model variables on a R^2 plot."""
    patch_ids = patches.Patch(color=plt.cm.tab20(0), label="RS GPP")
    patch_lue = patches.Patch(color=plt.cm.tab20(18), label="RS LUE")
    patch_sif = patches.Patch(color=plt.cm.tab20(4), label="SIF")
    patch_fluxcom_gpp = patches.Patch(
        color=plt.cm.tab20(2), label="FluxCom GPP"
    )
    patch_fluxcom_nee = patches.Patch(
        color=plt.cm.tab20(3), label="FluxCom NEE"
    )
    patch_mstmip_v1_gpp = patches.Patch(
        color=plt.cm.tab20(8), label="MsTMIP v1 GPP"
    )
    patch_mstmip_v1_nee = patches.Patch(
        color=plt.cm.tab20(9), label="MsTMIP v1 NEE"
    )
    patch_mstmip_v2_gpp = patches.Patch(
        color=plt.cm.tab20(12), label="MsTMIP v2 GPP"
    )
    patch_mstmip_v2_nee = patches.Patch(
        color=plt.cm.tab20(13), label="MsTMIP v2 NEE"
    )
    patch_trendy_v5_gpp = patches.Patch(
        color=plt.cm.tab20(16), label="TRENDY v5 GPP"
    )
    # patch_trendy_v5_nee = patches.Patch(
    #     color=plt.cm.tab20(17), label="TRENDY v5 NEE"
    # )
    patch_trendy_v6_gpp = patches.Patch(
        color=plt.cm.tab20(10), label="TRENDY v6 GPP"
    )
    patch_trendy_v6_nee = patches.Patch(
        color=plt.cm.tab20(11), label="TRENDY v6 NEE"
    )
    marker_blank = lines.Line2D([0], [0], color="none")  # a blank placeholder
    return [
        patch_ids,
        patch_lue,
        patch_sif,
        patch_fluxcom_gpp,
        patch_fluxcom_nee,
        patch_mstmip_v1_gpp,
        patch_mstmip_v1_nee,
        patch_mstmip_v2_gpp,
        patch_mstmip_v2_nee,
        patch_trendy_v5_gpp,
        marker_blank,  # patch_trendy_v5_nee,
        patch_trendy_v6_gpp,
        patch_trendy_v6_nee,
    ]


def _make_legend_symbols(size: Optional[float] = None):
    """Make legend handles for model variables on a R^2 plot."""
    marker_ids = lines.Line2D(
        [0],
        [0],
        marker="o",
        color=plt.cm.tab20(0),
        markerfacecolor="none",
        markersize=size,
        label="RS GPP",
    )
    marker_lue = lines.Line2D(
        [0],
        [0],
        marker="<",
        color=plt.cm.tab20(18),
        markerfacecolor="none",
        markersize=size,
        label="RS LUE",
    )
    marker_sif = lines.Line2D(
        [0],
        [0],
        marker=">",
        color=plt.cm.tab20(4),
        markerfacecolor="none",
        markersize=size,
        label="SIF",
    )
    marker_fluxcom_gpp = lines.Line2D(
        [0],
        [0],
        marker="o",
        color=plt.cm.tab20(2),
        markerfacecolor="none",
        markersize=size,
        label="FluxCom GPP",
    )
    marker_fluxcom_nee = lines.Line2D(
        [0],
        [0],
        marker="s",
        color=plt.cm.tab20(3),
        markerfacecolor="none",
        markersize=size,
        label="FluxCom NEE",
    )
    marker_mstmip_v1_gpp = lines.Line2D(
        [0],
        [0],
        marker="o",
        color=plt.cm.tab20(8),
        markerfacecolor="none",
        markersize=size,
        label="MsTMIP v1 GPP",
    )
    marker_mstmip_v1_nee = lines.Line2D(
        [0],
        [0],
        marker="s",
        color=plt.cm.tab20(9),
        markerfacecolor="none",
        markersize=size,
        label="MsTMIP v1 NEE",
    )
    marker_mstmip_v2_gpp = lines.Line2D(
        [0],
        [0],
        marker="o",
        color=plt.cm.tab20(12),
        markerfacecolor="none",
        markersize=size,
        label="MsTMIP v2 GPP",
    )
    marker_mstmip_v2_nee = lines.Line2D(
        [0],
        [0],
        marker="s",
        color=plt.cm.tab20(13),
        markerfacecolor="none",
        markersize=size,
        label="MsTMIP v2 NEE",
    )
    marker_trendy_v5_gpp = lines.Line2D(
        [0],
        [0],
        marker="o",
        color=plt.cm.tab20(16),
        markerfacecolor="none",
        markersize=size,
        label="TRENDY v5 GPP",
    )
    # marker_trendy_v5_nee = lines.Line2D(
    #     [0],
    #     [0],
    #     marker="s",
    #     color=plt.cm.tab20(17),
    #     markerfacecolor="none",
    #     markersize=size,
    #     label="TRENDY v5 NEE",
    # )
    marker_trendy_v6_gpp = lines.Line2D(
        [0],
        [0],
        marker="o",
        color=plt.cm.tab20(10),
        markerfacecolor="none",
        markersize=size,
        label="TRENDY v6 GPP",
    )
    marker_trendy_v6_nee = lines.Line2D(
        [0],
        [0],
        marker="s",
        color=plt.cm.tab20(11),
        markerfacecolor="none",
        markersize=size,
        label="TRENDY v6 NEE",
    )
    marker_blank = lines.Line2D([0], [0], color="none")  # a blank placeholder
    return [
        marker_ids,
        marker_lue,
        marker_sif,
        marker_fluxcom_gpp,
        marker_fluxcom_nee,
        marker_mstmip_v1_gpp,
        marker_mstmip_v1_nee,
        marker_mstmip_v2_gpp,
        marker_mstmip_v2_nee,
        marker_trendy_v5_gpp,
        marker_blank,  # marker_trendy_v5_nee,
        marker_trendy_v6_gpp,
        marker_trendy_v6_nee,
    ]


MARKER_STYLES = _make_marker_styles()
AXIS_LABELS = _make_axis_labels()
LEGEND_HANDLES = _make_legend_handles()


def query_style(v: str) -> Tuple[str, str, float, float]:
    """Return marker style and position for the variable on an R^2 plot."""
    var_names = MARKER_STYLES["var_name"].to_list()
    if v in var_names:
        idx = var_names.index(v)
        return (
            MARKER_STYLES.loc[idx, "color"],
            MARKER_STYLES.loc[idx, "marker"],
            MARKER_STYLES.loc[idx, "markersize"],
            MARKER_STYLES.loc[idx, "position"],
        )
    else:
        return (
            MARKER_STYLES.loc[0, "color"],
            MARKER_STYLES.loc[0, "marker"],
            MARKER_STYLES.loc[-1, "markersize"],
            MARKER_STYLES["position"].max() + 1,
        )


def annotate_bracket(
    ax,
    points_x,
    points_y,
    text,
    linecolor="k",
    ha="center",
    va="bottom",
    clip_on=False,
    **kwargs
) -> None:
    """Annotate text on a plot with a bracket."""
    ax.plot(points_x, points_y, "k-", lw=1, c=linecolor, clip_on=clip_on)
    text_pos_x = (points_x[1] + points_x[2]) * 0.5
    text_pos_y = (points_y[1] + points_y[2]) * 0.5
    ax.text(text_pos_x, text_pos_y, text, ha=ha, va=va, **kwargs)


# def plot_r2(
#     ax, df, var_name_col: str, errorbar: bool = True, y_grid: bool = True,
# ) -> None:
#     """Plot model R^2 values on a subplot."""
#     for i in df.index:
#         v = df.loc[i, var_name_col]
#         c, marker, markersize, pos = query_style(v)
#         r2 = df.loc[i, "r2"]
#         ax.scatter(
#             r2,
#             pos,
#             marker=marker,
#             s=markersize,
#             c="none",
#             edgecolor=c,
#             zorder=4,
#         )
#         if errorbar:
#             r2_err = (
#                 (df.loc[i, ["r2_lwr", "r2_upr"]] - df.loc[i, "r2"])
#                 .abs()
#                 .values.reshape(-1, 1)
#             )
#             ax.errorbar(
#                 r2,
#                 pos,
#                 xerr=r2_err,
#                 fmt="",
#                 linestyle="none",
#                 ecolor=c,
#                 capsize=1.0,
#                 elinewidth=0.75,
#                 zorder=3,
#             )

#     ytick_locs = AXIS_LABELS["position"].values  # var_pos_unique
#     yticklabels = AXIS_LABELS["label"].values  # var_names_unique
#     ax.set_yticks(ytick_locs)
#     ax.set_yticklabels(yticklabels, fontsize=8)
#     ax.invert_yaxis()  # labels read from top to bottom
#     if y_grid:
#         ax.grid(axis="y", linestyle=":", zorder=-1, color=plt.cm.tab20c(15))
