# NASA IDS Project
#
# Copyright (c) 2019 Wu Sun <wsun@carnegiescience.edu>
# Michalak Lab
# Department of Global Ecology
# Carnegie Institution for Science
"""Grid configuration and functions."""
from typing import Dict, List, Optional, Tuple

import numpy as np
import pandas as pd

from config import dirs
from .data_io import read_mat


def _read_old_NA_coords() -> pd.DataFrame:
    """Read coordinates of the old North American grid."""
    path: str = f"{dirs.coords_data}/lat-lon-old-na-grid.csv"
    coords: pd.DataFrame = pd.read_csv(path, encoding="utf-8", engine="c")
    return coords


def _read_new_NA_mask() -> np.ndarray:
    """Read the mask array used to select new grid points from the old grid."""
    path: str = f"{dirs.coords_data}/mask_NA_2635_to_2613.mat"
    mat: Optional[Dict[str, np.ndarray]] = read_mat(path)
    if mat is None:
        raise ValueError(f"Error when reading new grid mask file: {path}!")
    mask: np.ndarray = mat["mask"].flatten().astype("bool")
    return mask


def _read_new_NA_index() -> np.ndarray:
    """Read the grid index array used to rearrange the new grid points."""
    path: str = f"{dirs.coords_data}/index_NA_2613_shuffle.mat"
    mat: Optional[Dict[str, np.ndarray]] = read_mat(path)
    if mat is None:
        raise ValueError(f"Error when reading new grid index file: {path}!")
    index: np.ndarray = mat["index"].flatten().astype("int") - 1  # to py index
    return index


N_OLD_NA_GRID: int = 2635
N_NEW_NA_GRID: int = 2613
_coords: pd.DataFrame = _read_old_NA_coords()
_mask: np.ndarray = _read_new_NA_mask()
_index: np.ndarray = _read_new_NA_index()


def convert_NA_grid(
    x: np.ndarray,
    mask: Optional[np.ndarray] = None,
    index: Optional[np.ndarray] = None,
) -> np.ndarray:
    """
    Convert the grid of the North American domain to a new 2613-point grid.

    The input array ``x`` must be a stack of column vectors arranged as
    [*longitude*, *latitude*, *data*]. This function requires the grid mask
    (old to new) and the new grid index to be read from ``../data/coords/``
    (the default) or to be given by ``mask`` and ``index``.
    """
    if x is None or len(x.shape) != 2:
        raise ValueError("Input array is not a column vector or matrix!")
    # use the default grid mask and index if the arguments are not given
    if mask is None:
        mask = _mask
    if index is None:
        index = _index
    n_grid: int = N_OLD_NA_GRID
    # number of repetitions of the spatial domain
    n_reps: int = x.shape[0] // n_grid
    if n_reps == 0:
        raise ValueError("Input array has too few grid points (n < n_grid)!")
    # truncate x as multiples of the old grid domain
    x_trunc: np.ndarray = x[0 : n_grid * n_reps, :]
    if n_reps == 1:
        x_selected: np.ndarray = x_trunc[mask, :]
        x_reordered: np.ndarray = x_selected[index, :]
    else:
        # created repeated masks and indices
        n_new_grid: int = N_NEW_NA_GRID
        mask_repeated: np.ndarray = np.tile(mask, n_reps)
        index_repeated: np.ndarray = (
            np.tile(index, n_reps)
            + np.repeat(np.arange(n_reps), n_new_grid) * n_new_grid
        )
        x_selected = x_trunc[mask_repeated, :]
        x_reordered = x_selected[index_repeated, :]
    return x_reordered


def get_NA_grid_index(
    edge_grid: bool = False,
) -> Tuple[np.ndarray, np.ndarray, Tuple[np.ndarray, np.ndarray]]:
    """Get coords and index of land cells in a 2-D North American grid."""
    # use the old grid to generate the new grid
    coords_arr: np.ndarray = _coords.values
    # convert to the new grid coordinates
    coords_arr_new: np.ndarray = convert_NA_grid(coords_arr)
    lats: np.ndarray = coords_arr_new[:, 0].reshape(-1, 1)  # column vector
    lons: np.ndarray = coords_arr_new[:, 1].reshape(-1, 1)  # column vector
    # generate grid and identify indices
    min_lat, max_lat = lats.min(), lats.max()
    min_lon, max_lon = lons.min(), lons.max()
    lon_grid, lat_grid = np.meshgrid(
        np.arange(min_lon, max_lon + 0.5), np.arange(min_lat, max_lat + 0.5)
    )
    index_i: List[int] = []
    index_j: List[int] = []
    # match grid point by point
    for lon, lat in zip(lons, lats):
        i_arr, j_arr = np.where((lon_grid == lon) & (lat_grid == lat))
        index_i.append(i_arr.item())
        index_j.append(j_arr.item())
    # create grid index
    grid_index: Tuple[np.ndarray, np.ndarray] = (
        np.array(index_i),
        np.array(index_j),
    )
    # if edge_grid is set True, return edges of grid cells (for pcolormesh)
    if edge_grid:
        lon_grid_edges, lat_grid_edges = np.meshgrid(
            np.arange(min_lon - 0.5, max_lon + 1.0),
            np.arange(min_lat - 0.5, max_lat + 1.0),
        )
        return lon_grid_edges, lat_grid_edges, grid_index
    else:
        return lon_grid, lat_grid, grid_index
