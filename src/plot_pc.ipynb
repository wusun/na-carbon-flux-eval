{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Plot principal component patterns\n",
    "\n",
    "Wu Sun (wsun@carnegiescience.edu)\n",
    "\n",
    "* Created on 2019-11-07\n",
    "* Updated on 2021-02-12\n",
    "* Adapted for release on 2021-05-28"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Import packages"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import cartopy.crs as ccrs\n",
    "import cartopy.feature as cfeature\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import xarray as xr\n",
    "from matplotlib.gridspec import GridSpec, GridSpecFromSubplotSpec"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from config import dirs\n",
    "from libs.grids import get_NA_grid_index\n",
    "\n",
    "LON_GRID, LAT_GRID, GRID_INDEX = get_NA_grid_index()\n",
    "LON_GRID_EDGES, LAT_GRID_EDGES, _ = get_NA_grid_index(edge_grid=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.rcParams[\"figure.dpi\"] = 100\n",
    "plt.rcParams[\"savefig.dpi\"] = 300\n",
    "plt.rcParams[\"pdf.fonttype\"] = 42  # enforce TrueType fonts\n",
    "plt.rcParams[\"ps.fonttype\"] = 42"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Read data sets"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds_pc1 = xr.open_dataset(f\"{dirs.data}/gpp-nee-pc1.nc\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Figure 3: The normalized June–July–August mean PC1 patterns"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Principal component analysis** is used to extract the salient spatial and temporal (seasonal) features from multiple model simulations in multiple years. The rows are ordered as\n",
    "\n",
    "$$M_{1, y_1}, M_{2, y_1}, \\ldots, M_{k, y_1}, M_{1, y_2}, \\ldots, M_{k, y_p}$$\n",
    "\n",
    "where $M_i$ indicates the $i$-th model among $k$ models and $y_j$ indicates the $j$-th year among $p$ years.\n",
    "\n",
    "The columns are ordered as \n",
    "\n",
    "$$x_{1, t_1}, x_{2, t_1}, \\ldots, x_{n, t_1}, x_{1, t_2}, \\ldots, x_{m, t_n}$$\n",
    "\n",
    "where $x_i$ indicates the $i$-th spatial coordinate and $t_j$ indicates the $j$-th timestep within a year.\n",
    "\n",
    "**Visualization**\n",
    "\n",
    "The following figure presents summer mean patterns of the first principal components from model groups binned by _R_<sup>2</sup> performance.\n",
    "\n",
    "Note: The projected area is configured to center on the corn belt, and conforms roughly to the USGS Albers Equal Area projection.\n",
    "* Use 20°N and 60°N as the standard parallels for North America, or 29.5°N and 45.5°N for the conterminous US.\n",
    "* The longitude of origin is 96°W.\n",
    "* The latitude of origin recommended by NAD83 is 37.5°N. Here, it is changed it to 43.5°N to focus on the corn belt."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "proj = ccrs.AlbersEqualArea(-96, 43.5, standard_parallels=(20, 60))\n",
    "extent = [-132.0, -60.0, 6.0, 81.0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(6, 6.6))\n",
    "gs_rows = GridSpec(2, 1, height_ratios=[1, 1])\n",
    "gs_row1 = GridSpecFromSubplotSpec(1, 2, subplot_spec=gs_rows[0])\n",
    "gs_row2 = GridSpecFromSubplotSpec(1, 2, subplot_spec=gs_rows[1])\n",
    "\n",
    "ax1 = fig.add_subplot(gs_row1[0], projection=proj)\n",
    "ax2 = fig.add_subplot(gs_row1[1], projection=proj)\n",
    "ax3 = fig.add_subplot(gs_row2[0], projection=proj)\n",
    "ax4 = fig.add_subplot(gs_row2[1], projection=proj)\n",
    "axes = [ax1, ax2, ax3, ax4]\n",
    "\n",
    "# set map extents\n",
    "for ma in axes:\n",
    "    ma.add_feature(cfeature.COASTLINE, zorder=3, linewidth=0.75)\n",
    "    ma.add_feature(cfeature.BORDERS, zorder=3, linewidth=0.75)\n",
    "    states_provinces = cfeature.NaturalEarthFeature(\n",
    "        category=\"cultural\",\n",
    "        name=\"admin_1_states_provinces_lines\",\n",
    "        scale=\"50m\",\n",
    "        facecolor=\"none\",\n",
    "    )\n",
    "    ma.add_feature(states_provinces, edgecolor=\"C7\", zorder=3, linewidth=0.5)\n",
    "    ma.add_feature(\n",
    "        cfeature.LAKES, edgecolor=\"k\", facecolor=\"w\", zorder=3, linewidth=0.75\n",
    "    )\n",
    "    ma.set_extent(extent)\n",
    "\n",
    "pc1_var_pcts = []\n",
    "n_models = [20, 24, 12, 20]  # number of models\n",
    "for ma, group in zip(axes, [\"gpp_high_r2\", \"gpp_low_r2\", \"nee_high_r2\", \"nee_low_r2\"],):\n",
    "    pc1_var_pcts.append(ds_pc1[group].attrs[\"variance_explained\"] * 100.0)\n",
    "    da_pc1_jja = ds_pc1[group].sel(month=[6, 7, 8]).mean(\"month\")\n",
    "    vmin = da_pc1_jja.values.min()\n",
    "    vmax = da_pc1_jja.values.max()\n",
    "    # normalize the jja mean values to within the range of [0, 1]\n",
    "    da_pc1_jja_normalized = (da_pc1_jja - vmin) / (vmax - vmin)\n",
    "\n",
    "    data_crs = ccrs.PlateCarree()\n",
    "    z_grid = np.zeros_like(LON_GRID) + np.nan\n",
    "    z_grid[GRID_INDEX] = da_pc1_jja_normalized.values\n",
    "    cp = ma.pcolormesh(\n",
    "        LON_GRID_EDGES,\n",
    "        LAT_GRID_EDGES,\n",
    "        z_grid,\n",
    "        transform=data_crs,\n",
    "        vmin=0.0,\n",
    "        vmax=1.0,\n",
    "        zorder=2,\n",
    "        cmap=\"viridis\",\n",
    "        rasterized=True,\n",
    "    )\n",
    "\n",
    "axes[0].set_title(\"High $R^2$\", fontsize=10)\n",
    "axes[1].set_title(\"Low $R^2$\", fontsize=10)\n",
    "\n",
    "fig.text(0.03, 0.745, \"GPP & SIF\", ha=\"left\", va=\"center\", rotation=90, fontsize=10)\n",
    "fig.text(0.03, 0.335, \"NEE\", ha=\"left\", va=\"center\", rotation=90, fontsize=10)\n",
    "\n",
    "fig.canvas.draw()\n",
    "fig.tight_layout(h_pad=0.0, w_pad=0.0, rect=(0.04, 0.08, 1.0, 1.0))\n",
    "fig.subplots_adjust(wspace=0.05)\n",
    "# add panel labels\n",
    "for i, (pct, n_model) in enumerate(zip(pc1_var_pcts, n_models)):\n",
    "    ax = axes[i]\n",
    "    ax.text(\n",
    "        0.035,\n",
    "        0.055,\n",
    "        \"(%s) %.1f%% ($N = %i$)\" % (chr(i + 97), pct, n_model),\n",
    "        ha=\"left\",\n",
    "        fontsize=10,\n",
    "        transform=ax.transAxes,\n",
    "    )\n",
    "\n",
    "# add a common colorbar axis\n",
    "cax = fig.add_axes([0.12, 0.09, 0.8, 0.02])\n",
    "cb = fig.colorbar(cp, cax=cax, orientation=\"horizontal\")\n",
    "cb.set_label(\"J-J-A mean of PC1, normalized\", va=\"top\", fontsize=10)\n",
    "\n",
    "fig.savefig(f\"{dirs.plots}/fig-3-pc1-summer-mean.pdf\", dpi=300)"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Edit Metadata",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": true,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  },
  "toc-autonumbering": true,
  "toc-showcode": false,
  "toc-showmarkdowntxt": false,
  "toc-showtags": true
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
