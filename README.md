# Data and code for the evaluation of North American biospheric carbon flux estimates, published in _AGU Advances_

Wu Sun (wsun@carnegiescience.edu)

Department of Global Ecology, Carnegie Institution for Science

Last updated on 2021-06-25

## Summary

This repository presents data and code that were used to produce the main
results in the following publication:

Sun, W., Fang, Y., Luo, X., Shiga, Y. P., Zhang, Y., Andrews, A. E., Thoning,
K. W., Fisher, J. B., Keenan, T. F., & Michalak, A. M. (2021). Midwest US
croplands determine model divergence in North American carbon fluxes.
_AGU Advances_, 2, e2020AV000310. <https://doi.org/10.1029/2020AV000310>

<details>
  <summary>BibTeX citation</summary>

```bibtex
@article{sun_et_al_2021_aguadv,
  author  = {Sun, Wu and Fang, Yuanyuan and Luo, Xiangzhong and Shiga, Yoichi P. and Zhang, Yao and Andrews, Arlyn E. and Thoning, Kirk W. and Fisher, Joshua B. and Keenan, Trevor F. and Michalak, Anna M.},
  title   = {Midwest {US} croplands determine model divergence in {N}orth {A}merican carbon fluxes},
  journal = {AGU Advances},
  volume  = {2},
  number  = {2},
  pages   = {e2020AV000310},
  doi     = {10.1029/2020AV000310},
  url     = {https://agupubs.onlinelibrary.wiley.com/doi/10.1029/2020AV000310},
  year    = {2021}
}
```
</details>

## Data

Model evaluation results and supporting data are in the `data` folder.

* `H-monthly-summed.nc`: Monthly summed transport footprints (i.e., influence
  functions) that link point measurements of concentrations with fluxes.
* `H-nee-inv.csv`: Transported signals of geostatistical inverse estimates of
  NEE.
* `biome-id-obs.csv`: ID of the dominant biome in the footprint associated with
  each observation.
* `coords`: Coordinates of the North American land mask.
* `biomes.nc`: North American biome map used in this study.
* `gpp-nee-pc1.nc`: First principal component patterns of GPP and NEE extracted
  from model groups separated by the explanatory power of APAR.
* `lm-1var.csv`: Model benchmarking results.
* `lm-biome-month-null-exp.csv`: Results from biome-month encoded null
  experiments.
* `lm-biome-month.csv`: Results from biome-month encoded regressions.
* `seasonal-cycles-adjusted.nc`: Regression-adjusted seasonal cycles of carbon
  flux estimates from models.
* `sites.csv`: A list of tower sites where atmospheric CO<sub>2</sub>
  measurements were made.

## Code

Python code and Jupyter Notebooks to reproduce figures in the manuscript are in
the `src` folder. You need the following prerequisites to run the code.

* Python >= 3.8 (Python 2.x not supported)
* Jupyter Notebook
* numpy
* scipy
* pandas
* xarray
* matplotlib
* seaborn
* cartopy

The codebase is organized as follows:

* `config`: Configuration.
* `libs`: Custom functions for data processing and plotting.
* `plot_biomes.ipynb`: Plot the North American biome map in Figure 1.
* `plot_pc.ipynb`: Plot the summer mean PC1 patterns in Figure 3.
* `plot_regs.ipynb`: Plot model evaluation results in Figures 2, 4, and 5.

## License

This repository is dual-licensed under the [CC-BY-4.0 License](./LICENSE-CC-BY)
or the [MIT License](./LICENSE-MIT). You may choose either of them if you use
this work.

`SPDX-License-Identifier: CC-BY-4.0 OR MIT`

You do not need written permission from the authors to use this repository.

Please contact Wu Sun (wsun@carnegiescience.edu) if you have any questions.
